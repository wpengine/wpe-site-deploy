#!/bin/bash

set -e

latestRelease="$(curl -sL https://api.github.com/repos/wpengine/site-deploy/releases/latest)"
releaseNotesUrl="$(echo "$latestRelease" | jq -r ".html_url")"
newImageVersion="$(echo "$latestRelease" | jq -r ".tag_name" | tr -d v)"
oldImageVersion="$(grep "wpengine/site-deploy" pipe.yml | cut -f3 -d ":")"

# We will only handle versions in the format {MAJOR}.{MINOR}?.{PATCH}?
#   capture 1: major number
#   capture 2: minor number with dot
#   capture 3: minor number without dot
#   capture 4: patch number with dot
#   capture 5: patch number without dot
VERSION_REGEX="^(0|[1-9][0-9]*)(\.(0|[1-9][0-9]*)){0,1}(\.(0|[1-9][0-9]*)){0,1}$"

if [[ "$newImageVersion" =~ $VERSION_REGEX ]] ; then
    majorTag="v${BASH_REMATCH[1]}"
    minorTag="$majorTag.${BASH_REMATCH[3]:-0}"
    patchTag="$minorTag.${BASH_REMATCH[5]:-0}"
else
    echo "Error parsing new version number. Skipping updates."
    exit 0
fi

if [[ "$oldImageVersion" =~ $VERSION_REGEX ]] ; then
    oldMajorTag="v${BASH_REMATCH[1]}"
else
    echo "Error parsing old version number. Skipping updates."
    exit 0
fi

# Update files
sed -i '' -e "s/$oldImageVersion/$newImageVersion/" pipe.yml
sed -i '' -e "s/:$oldMajorTag/:$majorTag/" README.md
# Add changelog entry
message="## $patchTag\n - Update wpengine/site-deploy image to [$patchTag]($releaseNotesUrl)\n\n"
sed -i '' -e "5s!^!$message!g" CHANGELOG.md
