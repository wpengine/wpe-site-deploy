# Development

Most changes to this pipe should be done in the underlying Docker image. See [wpengine/site-deploy](https://github.com/wpengine/site-deploy/blob/main/DEVELOPMENT.md) for more information.

## Releasing

When a new version of `wpengine/site-deploy` is available, update this pipe using the following steps:
  1. Create a new branch.
  2. Run `bash scripts/bump-version.sh`.
  3. Commit, push, and open a PR.
  4. Once the PR is merged, run the "custom: release" pipeline from the Bitbucket UI to update tags.
