# CHANGELOG

Most updates to this pipe will be made in the underlying Docker image. See [wpengine/site-deploy](https://github.com/wpengine/site-deploy/releases) for full changelogs.

## v1.0.5
 - Update wpengine/site-deploy image to [v1.0.5](https://github.com/wpengine/site-deploy/releases/tag/v1.0.5)

## v1.0.4
 - Update wpengine/site-deploy image to [v1.0.4](https://github.com/wpengine/site-deploy/releases/tag/v1.0.4)

## v1.0.3
 - Update wpengine/site-deploy image to [v1.0.3](https://github.com/wpengine/site-deploy/releases/tag/v1.0.3)

## v1.0.2
 - Update wpengine/site-deploy image to [v1.0.2](https://github.com/wpengine/site-deploy/releases/tag/v1.0.2)

## v1.0.1
 - Update wpengine/site-deploy image to [v1.0.1](https://github.com/wpengine/site-deploy/releases/tag/v1.0.1)

## v1.0.0
 - Update wpengine/site-deploy image to [v1.0.0](https://github.com/wpengine/site-deploy/releases/tag/v1.0.0)
