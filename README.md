# Bitbucket Pipelines Pipe: WP Engine Site Deploy

Use this pipe to deploy code from a Bitbucket repo to a WP Engine environment of your choosing. If you do not have a WP Engine Account, [click here to get started!](https://wpengine.com/plans/?utm_content=wpe_bb)

This pipe enables you to:

  * Deploy a full site directory or subdirectory of your WordPress install
  * Perform a PHP Lint
  * Customize rsync flags
  * Clear cache
  * Execute a post-deploy script of your choosing

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: wpengine/wpe-site-deploy:v1
  variables:
    WPE_SSHG_KEY_PRIVATE: $WPE_SSHG_KEY_PRIVATE
    WPE_ENV: '<sitename>'
    # SRC_PATH: 'wp-content/themes/child-theme/' # Optional.
    # REMOTE_PATH: 'wp-content/themes/child-theme/' # Optional.
    # PHP_LINT: 'TRUE' # Optional.
    # FLAGS: '-azvr --inplace --delete --exclude=".*"' # Optional.
    # SCRIPT: 'wp-content/themes/child-theme/post-deploy.sh' # Optional.
    # CACHE_CLEAR: 'FALSE' # Optional.
```

## Variables

| Variable                      | Usage                                                |
| ----------------------------- | ---------------------------------------------------- |
| WPE_SSHG_KEY_PRIVATE (*)      | base64 encoded private SSH Key for the SSH Gateway and deployment. See [Prerequisites](#prerequisites) for more on SSH key setup. |
| WPE_ENV (*)                   | The name of the WP Engine environment you want to deploy to.
| SRC_PATH                      | Path to a file or folder within the repo that you wish to deploy. Pipe will deploy the entire repo by default. |
| REMOTE_PATH                   | Path on the server to use as the deployment target. Pipe will deploy to WP root by default. |
| PHP_LINT                      | Set to TRUE to execute a PHP lint on your branch pre-deployment. Default is `FALSE`. |
| FLAGS                         | Set optional rsync flags such as `--delete` or `--exclude-from`. This pipe defaults to a non-destructive deploy using the flags `-azvr --inplace --exclude=".*"`.<br /><br />_Caution: Setting custom rsync flags replaces the default flags provided by this pipe. Consider also adding the `-azvr` flags as needed.<br /> `-a` preserves symbolic links, timestamps, user permissions and ownership.<br /> `-z` is for compression <br /> `-v` is for verbose output<br /> `-r` is for recursive directory scanning_ <br/><br/> Read more about options on the [rsync man page](https://manpages.ubuntu.com/manpages/trusty/en/man1/rsync.1.html) |
| SCRIPT                        | Path to a bash script file to execute post-deploy. This can include WP CLI commands for example. Path is relative to the WP root and file executes on the server. This file can be included in your repo, or be a persistent file that lives on your server. |
| CACHE_CLEAR                   | Optionally clear page and CDN cache post deploy. This takes a few seconds. Default is TRUE. |

_(*) = required variable._

## Prerequisites

This pipe uses rsync to deploy your code through [WP Engine's SSH Gateway](https://wpengine.com/support/ssh-gateway/). Before deploying, you'll need to configure your SSH keys.

1. **Setup SSH public key in WP Engine**

* [Generate a new SSH key pair](https://wpengine.com/support/ssh-keys-for-shell-access/#Generate_New_SSH_Key?utm_content=wpe_bb) if you have not already done so. Please note that this SSH Key needs to be *passwordless*.
* Add your *SSH Public Key* to your [user profile](https://my.wpengine.com/profile/ssh_keys?utm_content=wpe_bb) in WP Engine's User Portal. [This guide will show you how.](https://wpengine.com/support/ssh-gateway/#Add_SSH_Key?utm_content=wpe_bb)

2. **Setup SSH private key in Bitbucket**

* Add the base64 encoded *SSH Private Key* as a secure workspace or repository or variable. [This guide will show you how](https://support.atlassian.com/bitbucket-cloud/docs/use-multiple-ssh-keys-in-your-pipeline/). Save the new secret "Name" as `WPE_SSHG_KEY_PRIVATE`.

## Examples

### Basic example:
Use this example to deploy your `main` branch to your production environment. Replace `<sitename>` with your production environment name and replace `main` with the name of your primary branch if it differs.


```yaml
pipelines:
  branches:
    main:
      - step:
          name: Deploy to Production
          script:
          - pipe: wpengine/wpe-site-deploy:v1
            variables:
              WPE_SSHG_KEY_PRIVATE: $WPE_SSHG_KEY_PRIVATE
              WPE_ENV: '<sitename>'
```


### Advanced examples:
Here you will find a different environment is deployed for `main` and `staging`. Note also the `staging` branch is utilizing Bitbucket deployment environments variables, where the `WPE_ENV` variable has been declared to further secure the environment name. This is completely optional.

```yaml
pipelines:
  branches:
    main:
      - step:
          name: Deploy to Production
          script:
          - pipe: wpengine/wpe-site-deploy:v1
            variables:
              WPE_SSHG_KEY_PRIVATE: $WPE_SSHG_KEY_PRIVATE
              WPE_ENV: '<sitename>'
    staging:
      - step:
          name: Deploy to Staging
          deployment: staging
          script:
          - pipe: wpengine/wpe-site-deploy:v1
            variables:
              WPE_SSHG_KEY_PRIVATE: $WPE_SSHG_KEY_PRIVATE
              WPE_ENV: $WPE_ENV
```

## Further reading

* **NOTE:** This pipe DOES NOT utilize WP Engine GitPush or the GitPush SSH keys [found here.](https://wpengine.com/support/git/#Add_SSH_Key_to_User_Portal?utm_content=wpe_bb)
* It is recommended to leverage one of [WP Engine's .gitignore templates.](https://wpengine.com/support/git/#Add_gitignore?utm_content=wpe_bb)
* This pipe excludes several files and directories from the deploy by default. See the [exclude.txt](https://github.com/wpengine/site-deploy/blob/main/exclude.txt) for reference.

## Versioning

We follow [SemVer](https://semver.org/) for maintaining major, minor, and patch version tags. Patch tags (e.g. `v1.1.1`) are created for each release and will not move once created. Major tags (e.g. `v1`) and minor tags (e.g. `v1.1`) will be moved as necessary to remain in sync with their respective latest versions.

We recommend binding this pipe to the latest major tag so that you will receive backwards compatible updates.
